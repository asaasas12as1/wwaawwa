<?php

header('Content-Type: application/json');

class Post
{
    private $app__key = "secret";
    private $db;

    public function __construct()
    {
        $this->db = new PDO('mysql:host=localhost;dbname=api_test', '', '') or die('Нет подключения');

        $method = strtolower($_SERVER['REQUEST_METHOD']);

        switch ($method) {
            case 'post':
                $this->create();
                break;
            case 'put':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;
            case 'get':
                $this->show();
                break;
            default:
                
                http_response_code(501);
        }
    }

    public function create()
    {

        try {
            $app_key = $_POST['app_key'];
            $name = $_POST['name'];
            $description = $_POST['description'];
            $image = $_POST['image'];
            $userId = $_POST['userid'];


            if ($this->app__key != $app_key) {
                echo json_encode(array('error' => 'Error secret code'));
                return;
            }


            $sql = $this->db->prepare('INSERT INTO posts (name, description, image, author_id) VALUES (:name, :description, :image, :author_id)');
            $sql->bindParam(':name', $name);
            $sql->bindParam(':description', $description);
            $sql->bindParam(':image', $image);
            $sql->bindParam(':author_id', $userId);
            $sql->execute();

            http_response_code(201);
            echo json_encode(array('response' => 'Запись создана'));

        } catch (Exception $e)
        {
            echo json_encode(array('error' => 'Произошла ошибка, повторите запрос через 5 минут'));
        }
        return;

    }

    public function update()
    {
        try {
            $app_key = $_POST['app_key'];
            $name = $_POST['name'];
            $description = $_POST['description'];
            $image = $_POST['image'];
            $userId = $_POST['userid'];

            if ($this->app__key != $app_key) {
                echo json_encode(array('error' => 'Error secret code'));
                return;
            }

            $s = $this->db->prepare('SELECT * FROM posts WHERE author_id = :author_id and name = :name');
            $s->bindParam(':author_id', $userId);
            $s->bindParam(':name', $name);
            $s->execute();
            $result = $s->fetch();

            $sql = $this->db->prepare('UPDATE posts SET name =:name, description=:description, image =:image, WHERE id = :id,');
            $sql->bindParam(':name', $name);
            $sql->bindParam(':description', $description);
            $sql->bindParam(':image', $image);
            $sql->bindParam(':id', $result['id']);
            $sql->execute();

            echo json_encode($result);

            http_response_code(200);

        } catch (Exception $e) {
            echo json_encode(array('error' => 'Произошла ошибка, повторите запрос через 5 минут'));
        }
        return;
    }

    public function delete()
    {

        try {
            $app_key = $_GET['app_key'];
            $userId = $_GET['userid'];
            $name = $_GET['name'];

            if ($this->app__key != $app_key) {
                echo json_encode(array('error' => 'Error secret code'));
                return;
            }

            $sql = $this->db->prepare('DELETE FROM posts WHERE name = :name and author_id = :author_id');
            $sql->bindParam(':name', $name);
            $sql->bindParam(':author_id', $userId);
            $sql->execute();


            http_response_code(200);

        } catch (Exception $e) {
            echo json_encode(array('error' => 'Произошла ошибка, повторите запрос через 5 минут'));
        }
        return;

    }

    public function show()
    {

        try {
            $app_key = $_GET['app_key'];
            $userId = $_GET['userid'];

            if ($this->app__key != $app_key) {
                echo json_encode(array('error' => 'Error secret code'));
                return;
            }

            $sql = $this->db->prepare('SELECT * FROM posts WHERE author_id = :author_id');
            $sql->bindParam(':author_id', $userId);
            $sql->execute();
            $result = $sql->fetchAll();

            echo json_encode($result);

            http_response_code(200);

        } catch (Exception $e) {
            echo json_encode(array('error' => 'Произошла ошибка, повторите запрос через 5 минут'));
        }
        return;
    }




}

new Post();