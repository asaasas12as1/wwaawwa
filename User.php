<?php

header('Content-Type: application/json');

class User
{
    private $app__key = "secret";
    private $db;

    public function __construct()
    {
        $this->db = new PDO('mysql:host=localhost;dbname=api_test', '', '');


        $method = strtolower($_SERVER['REQUEST_METHOD']);

        switch($method) {
            case 'post':
                $this->create();
                break;
            case 'get':
                $this->show();
                break;
            default:
                http_response_code(405);
        }
    }


    public function create()
    {

        try {
            $app_key = $_POST['app_key'];
            $login = $_POST['login'];
            $password = $_POST['password'];
            $name = $_POST['name'];


            if ($this->app__key != $app_key) {
                echo json_encode(array('error' => 'Error secret code'));
                return;
            }

            if (mb_strlen($login) >= 15){
                echo json_encode(array('error' => 'Логин слишком длинный'));
                return;
            }


            $sql = $this->db->prepare('INSERT INTO users (login, password, name) VALUES (:login, :password, :name)');
            $sql->bindParam(':login', $login);
            $sql->bindParam(':password', $password);
            $sql->bindParam(':name', $name);
            $sql->execute();

            http_response_code(201);
            echo json_encode(array('response' => 'Пользователь создан'));

        } catch (Exception $e)
        {
            echo json_encode(array('error' => 'Произошла ошибка, повторите запрос через 5 минут'));
        }
        return;
    }

    public function show()
    {

        try {
            $app_key = $_GET['app_key'];
            $login = $_GET['login'];
            $password = $_GET['password'];

            if ($this->app__key != $app_key) {
                echo json_encode(array('error' => 'Error secret code'));
                return;
            }

            $sql = $this->db->prepare('SELECT * FROM users WHERE login = :login');
            $sql->bindParam(':login', $login);
            $sql->execute();
            $result = $sql->fetch();

            if ($login == $result['login'] && $password == $result['password']) {
                $user = array('id' => $result['id'], 'name' => $result['name']);
                echo json_encode($user);
            } else {
                echo json_encode(array('error' => 'Неверный логин или пароль'));
            }

            http_response_code(200);

        } catch (Exception $e)
        {
            echo json_encode(array('error' => 'Произошла ошибка, повторите запрос через 5 минут'));
        }
        return;
    }

}

new User();